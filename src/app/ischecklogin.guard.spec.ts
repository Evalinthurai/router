import { TestBed } from '@angular/core/testing';

import { IscheckloginGuard } from './ischecklogin.guard';

describe('IscheckloginGuard', () => {
  let guard: IscheckloginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(IscheckloginGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
