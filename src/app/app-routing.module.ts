import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginGuard } from './login.guard'
import { IscheckloginGuard } from './ischecklogin.guard'

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate:[LoginGuard]
  },
  {
    path: 'login',
    component : LoginComponent,
    canActivate:[IscheckloginGuard]
  },
  {
    path: '**',
    redirectTo: '' 
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
