import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router'

@Injectable({
  providedIn: 'root'
})
export class IscheckloginGuard implements CanActivate {
  constructor( private router: Router){
    
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let islogin = localStorage.getItem('logintoken');
    if(islogin==null|| islogin== ''){
        return true;
    } 
    this.router.navigate(['/']);
    return false;
  }
  
}
